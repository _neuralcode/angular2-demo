import { Component } from '@angular/core';

import { Note } from '../note/note.model';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.sass']
})
export class PeopleComponent {

  notes: Array<Note> = [
    new Note('Lee McGowan', 'Junior Developer', 'Person', 'Assigned - Internal, non-billable', true, 'ODSC', 'Resourcing Tool'),
    new Note('Brian Wilson', 'Senior Frontend Engineer', 'Person', 'Assigned - Internal, non-billable', true, 'ODSC', 'Bookit'),
    new Note('Jordan Antoniou', 'Undergraduate Developer', 'Person', 'Assigned - Billable', false, 'ODSC', 'DCA')
  ]

  filtered: Array<Note>;
  searchTerm: string;

  constructor() {
    this.searchTerm = "";
    this.filtered = this.notes;
  }

  filterNotes() {
    this.filtered = this.notes;
    this.filtered = this.filtered.filter(n => this.matches(n, this.searchTerm));
  }

  matches(n: Note, term: string) {
    const details = n.getDetails();
    term = term.toUpperCase();
    return (details.currentProject.toUpperCase().includes(term)) || 
           (details.heading.toUpperCase().includes(term)) || 
           (details.location.toUpperCase().includes(term)) || 
           (details.desc.toUpperCase().includes(term)); 
  }
};
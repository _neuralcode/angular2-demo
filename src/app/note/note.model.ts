export class Note {

  private heading: string;
  private desc: string;
  private type: string;
  private availability: string;
  private isAvailable: boolean;
  private location: string;
  private currentProject: string

  constructor(heading: string, desc: string, type: string, availability: string, isAvailable: boolean, location: string, currentProject: string) {
    this.heading = heading;
    this.desc = desc;
    this.type = type;
    this.isAvailable = isAvailable;
    this.location = location;
    this.currentProject = currentProject;
  }

  getDetails() {
    return {
      heading: this.heading,
      desc: this.desc,
      type: this.type,
      availability: this.availability,
      isAvailable: this.isAvailable,
      location: this.location,
      currentProject: this.currentProject
    }
  }
};
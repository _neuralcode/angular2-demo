import { Component } from '@angular/core';

import { Note } from '../note/note.model';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.sass']
})
export class ProjectsComponent {
  notes: Array<Note> = [
    new Note('Resourcing Tool', 'A tool to manage project resources', 'Project', null, null, 'ODSC', null),
    new Note('DCA', 'An integration layer for the smart metering programme', 'Project', null, null, 'ODSC', null),
    new Note('Bookit', 'A resource booking tool', 'Project', null, null, 'ODSC', null),
    new Note('ARE', 'Agricultural financial management application', 'Project', null, null, 'ODSC', null),
    new Note('Juror', 'Ministry of Justice online portal', 'Project', null, null, 'ODSC', null),
    new Note('Droned', 'Description unavailable', 'Project', null, null, 'ODSC', null),
  ];

  filtered: Array<Note>;
  searchTerm: string;

  constructor() {
    this.filtered = this.notes;
    this.searchTerm = '';
  }

  filterNotes() {
    this.filtered = this.notes;
    this.filtered = this.filtered.filter(n => this.matches(n, this.searchTerm));
  }

  matches(n: Note, term: string) {
    const details = n.getDetails();
    term = term.toUpperCase();
    return (details.heading.toUpperCase().includes(term)) || 
           (details.location.toUpperCase().includes(term)) || 
           (details.desc.toUpperCase().includes(term)); 
  }
}